package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FinalServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("customer", FirstServlet.customer);

		if (request.getParameter("next") != null) {

			try {
				request.getRequestDispatcher("index2.jsp").forward(request, response);
			} catch (IOException e) {
			}

		} else if (request.getParameter("prev") != null) {

			try {
				request.getRequestDispatcher("pit44.jsp").forward(request, response);
			} catch (IOException e) {
			}

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}

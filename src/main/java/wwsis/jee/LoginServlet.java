package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import wwsis.jee.data.UserProfile;

//u�ycie adnotacji do konfiguracji serwletu
@WebServlet(asyncSupported = false, urlPatterns="/login.do")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(LoginServlet.class);

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("Login Servlet - pobieram dane z formularza logowania");
		
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String name = request.getParameter("name");
		String surename = request.getParameter("surename");
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		
		UserProfile up = new UserProfile();
		up.setLogin(login);
		up.setName(name);
		up.setSurename(surename);
		up.setPass(pass);
		
		logger.info("Tworzenie cookie");
		Cookie ileRazy = createCookie(request, response);
	
		logger.info("Login Servlet - dodanie danych do sesji");
		request.getSession().setAttribute("userProfile", up);
		request.getSession().setAttribute("howMany", ileRazy);
		request.getRequestDispatcher("pit11.jsp").forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("Login Servlet - doGet method");
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("Login Servlet - doPost method");
		processRequest(request, response);
	}
	
	private Cookie createCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie ileRazy = null;

		if (request.getCookies() != null) {
			ileRazy = findCookie(request, "howMany");
		}
		
		if (ileRazy != null) {
			int counter = Integer.valueOf(ileRazy.getValue()).intValue();
			counter++;
			ileRazy = new Cookie("howMany", String.valueOf(counter));
			response.addCookie(ileRazy);
			System.out.println("HowMany = " + ileRazy);
		} else {
			ileRazy = new Cookie("howMany", "0");
			response.addCookie(ileRazy);
			System.out.println("HowMany = " + ileRazy);
		}
		return ileRazy;
	}

	private Cookie findCookie(HttpServletRequest request, String name){
		for (Cookie cookie : request.getCookies())
			if (cookie.getName().equals(name)){
				return cookie;
			}
		return null;
	}
}

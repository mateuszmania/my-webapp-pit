package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import wwsis.jee.data.MyData;

public class FirstServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	static MyData customer = new MyData();

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		customer.setPesel(request.getParameter("pesel"));
		customer.setPeselM(request.getParameter("peselM"));
		customer.setRok(request.getParameter("rok"));

		request.setAttribute("customer", customer);

		if (request.getParameter("next") != null) {

			try {
				request.getRequestDispatcher("pit22.jsp").forward(request, response);
			} catch (IOException e) {
			}

		} else if (request.getParameter("prev") != null) {

			try {
				request.getRequestDispatcher("index.jsp").forward(request, response);
			} catch (IOException e) {
			}

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}

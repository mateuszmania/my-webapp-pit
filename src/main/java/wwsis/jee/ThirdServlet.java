package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ThirdServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FirstServlet.customer.setNazwisko(request.getParameter("nazwisko"));
		FirstServlet.customer.setImie(request.getParameter("imie"));
		FirstServlet.customer.setData(request.getParameter("data"));
		FirstServlet.customer.setKraj(request.getParameter("kraj"));
		FirstServlet.customer.setWoj(request.getParameter("woj"));
		FirstServlet.customer.setPow(request.getParameter("pow"));
		FirstServlet.customer.setGmina(request.getParameter("gmina"));
		FirstServlet.customer.setUlica(request.getParameter("ulica"));
		FirstServlet.customer.setNrDomu(request.getParameter("nrDomu"));
		FirstServlet.customer.setNrLokalu(request.getParameter("nrLokalu"));
		FirstServlet.customer.setMiejscowosc(request.getParameter("miejscowosc"));
		FirstServlet.customer.setKod(request.getParameter("kod"));
		FirstServlet.customer.setPoczta(request.getParameter("poczta"));

		request.setAttribute("customer", FirstServlet.customer);

		if (request.getParameter("next") != null) {

			try {
				request.getRequestDispatcher("pit44.jsp").forward(request, response);
			} catch (IOException e) {
			}

		} else if (request.getParameter("prev") != null) {

			try {
				request.getRequestDispatcher("pit22.jsp").forward(request, response);
			} catch (IOException e) {
			}

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}

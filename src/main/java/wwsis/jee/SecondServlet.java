package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecondServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FirstServlet.customer.setOpodatkowanie1(request.getParameter("opodatkowanie1"));
		FirstServlet.customer.setOpodatkowanie2(request.getParameter("opodatkowanie2"));
		FirstServlet.customer.setOpodatkowanie3(request.getParameter("opodatkowanie3"));
		FirstServlet.customer.setOpodatkowanie4(request.getParameter("opodatkowanie4"));
		FirstServlet.customer.setOpodatkowanie5(request.getParameter("opodatkowanie5"));
		FirstServlet.customer.setOpodatkowanie6(request.getParameter("opodatkowanie6"));
		
		FirstServlet.customer.setUrzad(request.getParameter("urzad"));
		FirstServlet.customer.setCel(request.getParameter("cel"));
		FirstServlet.customer.setCel1(request.getParameter("cel1"));
		
		request.setAttribute("customer", FirstServlet.customer);

		if (request.getParameter("next") != null) {

			try {
				request.getRequestDispatcher("pit33.jsp").forward(request, response);
			} catch (IOException e) {
			}

		} else if (request.getParameter("prev") != null) {

			try {
				request.getRequestDispatcher("pit11.jsp").forward(request, response);
			} catch (IOException e) {
			}

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}

package wwsis.jee.data;

public class MyData {
	
	private String pesel;
	private String peselM;
	private String rok;
	private String opodatkowanie1;
	private String opodatkowanie2;
	private String opodatkowanie3;
	private String opodatkowanie4;
	private String opodatkowanie5;
	private String opodatkowanie6;
	private String urzad;
	private String cel;
	private String cel1;
	
	private String nazwisko;
	private String imie;
	private String data;
	private String kraj;
	private String woj;
	private String pow;
	private String gmina;
	private String ulica;
	private String nrDomu;
	private String nrLokalu;
	private String miejscowosc;
	private String kod;
	private String poczta;
	
	private String nazwiskoM;
	private String imieM;
	private String dataM;
	private String krajM;
	private String wojM;
	private String powM;
	private String gminaM;
	private String ulicaM;
	private String nrDomuM;
	private String nrLokaluM;
	private String miejscowoscM;
	private String kodM;
	private String pocztaM;
	
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getPeselM() {
		return peselM;
	}
	public void setPeselM(String peselM) {
		this.peselM = peselM;
	}
	public String getRok() {
		return rok;
	}
	public void setRok(String rok) {
		this.rok = rok;
	}
	public String getUrzad() {
		return urzad;
	}
	public void setUrzad(String urzad) {
		this.urzad = urzad;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getKraj() {
		return kraj;
	}
	public void setKraj(String kraj) {
		this.kraj = kraj;
	}
	public String getWoj() {
		return woj;
	}
	public void setWoj(String woj) {
		this.woj = woj;
	}
	public String getPow() {
		return pow;
	}
	public void setPow(String pow) {
		this.pow = pow;
	}
	public String getGmina() {
		return gmina;
	}
	public void setGmina(String gmina) {
		this.gmina = gmina;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getNrDomu() {
		return nrDomu;
	}
	public void setNrDomu(String nrDomu) {
		this.nrDomu = nrDomu;
	}
	public String getNrLokalu() {
		return nrLokalu;
	}
	public void setNrLokalu(String nrLokalu) {
		this.nrLokalu = nrLokalu;
	}
	public String getMiejscowosc() {
		return miejscowosc;
	}
	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}
	public String getKod() {
		return kod;
	}
	public void setKod(String kod) {
		this.kod = kod;
	}
	public String getPoczta() {
		return poczta;
	}
	public void setPoczta(String poczta) {
		this.poczta = poczta;
	}
	public String getNazwiskoM() {
		return nazwiskoM;
	}
	public void setNazwiskoM(String nazwiskoM) {
		this.nazwiskoM = nazwiskoM;
	}
	public String getImieM() {
		return imieM;
	}
	public void setImieM(String imieM) {
		this.imieM = imieM;
	}
	public String getDataM() {
		return dataM;
	}
	public void setDataM(String dataM) {
		this.dataM = dataM;
	}
	public String getKrajM() {
		return krajM;
	}
	public void setKrajM(String krajM) {
		this.krajM = krajM;
	}
	public String getWojM() {
		return wojM;
	}
	public void setWojM(String wojM) {
		this.wojM = wojM;
	}
	public String getPowM() {
		return powM;
	}
	public void setPowM(String powM) {
		this.powM = powM;
	}
	public String getGminaM() {
		return gminaM;
	}
	public void setGminaM(String gminaM) {
		this.gminaM = gminaM;
	}
	public String getUlicaM() {
		return ulicaM;
	}
	public void setUlicaM(String ulicaM) {
		this.ulicaM = ulicaM;
	}
	public String getNrDomuM() {
		return nrDomuM;
	}
	public void setNrDomuM(String nrDomuM) {
		this.nrDomuM = nrDomuM;
	}
	public String getNrLokaluM() {
		return nrLokaluM;
	}
	public void setNrLokaluM(String nrLokaluM) {
		this.nrLokaluM = nrLokaluM;
	}
	public String getMiejscowoscM() {
		return miejscowoscM;
	}
	public void setMiejscowoscM(String miejscowoscM) {
		this.miejscowoscM = miejscowoscM;
	}
	public String getKodM() {
		return kodM;
	}
	public void setKodM(String kodM) {
		this.kodM = kodM;
	}
	public String getPocztaM() {
		return pocztaM;
	}
	public void setPocztaM(String pocztaM) {
		this.pocztaM = pocztaM;
	}
	public String getCel() {
		return cel;
	}
	public void setCel(String cel) {
		this.cel = cel;
	}
	public String getCel1() {
		return cel1;
	}
	public void setCel1(String cel1) {
		this.cel1 = cel1;
	}
	public String getOpodatkowanie1() {
		return opodatkowanie1;
	}
	public void setOpodatkowanie1(String opodatkowanie1) {
		this.opodatkowanie1 = opodatkowanie1;
	}
	public String getOpodatkowanie2() {
		return opodatkowanie2;
	}
	public void setOpodatkowanie2(String opodatkowanie2) {
		this.opodatkowanie2 = opodatkowanie2;
	}
	public String getOpodatkowanie3() {
		return opodatkowanie3;
	}
	public void setOpodatkowanie3(String opodatkowanie3) {
		this.opodatkowanie3 = opodatkowanie3;
	}
	public String getOpodatkowanie4() {
		return opodatkowanie4;
	}
	public void setOpodatkowanie4(String opodatkowanie4) {
		this.opodatkowanie4 = opodatkowanie4;
	}
	public String getOpodatkowanie5() {
		return opodatkowanie5;
	}
	public void setOpodatkowanie5(String opodatkowanie5) {
		this.opodatkowanie5 = opodatkowanie5;
	}
	public String getOpodatkowanie6() {
		return opodatkowanie6;
	}
	public void setOpodatkowanie6(String opodatkowanie6) {
		this.opodatkowanie6 = opodatkowanie6;
	}

}


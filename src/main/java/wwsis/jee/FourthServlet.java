package wwsis.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FourthServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FirstServlet.customer.setNazwiskoM(request.getParameter("nazwiskoM"));
		FirstServlet.customer.setImieM(request.getParameter("imieM"));
		FirstServlet.customer.setDataM(request.getParameter("dataM"));
		FirstServlet.customer.setKrajM(request.getParameter("krajM"));
		FirstServlet.customer.setWojM(request.getParameter("wojM"));
		FirstServlet.customer.setPowM(request.getParameter("powM"));
		FirstServlet.customer.setGminaM(request.getParameter("gminaM"));
		FirstServlet.customer.setUlicaM(request.getParameter("ulicaM"));
		FirstServlet.customer.setNrDomuM(request.getParameter("nrDomuM"));
		FirstServlet.customer.setNrLokaluM(request.getParameter("nrLokaluM"));
		FirstServlet.customer.setMiejscowoscM(request.getParameter("miejscowoscM"));
		FirstServlet.customer.setKodM(request.getParameter("kodM"));
		FirstServlet.customer.setPocztaM(request.getParameter("pocztaM"));

		request.setAttribute("customer", FirstServlet.customer);

		if (request.getParameter("next") != null) {

			try {
				request.getRequestDispatcher("pit.jsp").forward(request, response);
			} catch (IOException e) {
			}

		} else if (request.getParameter("prev") != null) {

			try {
				request.getRequestDispatcher("pit33.jsp").forward(request, response);
			} catch (IOException e) {
			}

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PIT 37 (3 z 4)</title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="header.jsp"/>
</head>
<body>
<form action="third.do" method="post">
	<div class="content">
		<div class="blue" style="height: 25px; border-top: 1px solid black;">
			<a style="margin-left: 10px;">B. DANE IDENTYFIKACYJNE I AKTUALNY
				ADRES ZAMIESZKANIA</a>
		</div>
		<div class="blue" style="height: 172px">
			<div style="height: 28px;">
				<a style="margin-left: 15px; font-weight: normal;">B.1. DANE
					PODATNIKA</a>
			</div>
			<div class="white">
				<div class="white2"
					style="width: 300px; float: left; border-left: none;">
					<div class="header">11. Nazwisko</div>
					<div>
						<input type="text" name="nazwisko" value="${requestScope.customer.nazwisko}"/>
					</div>
				</div>
				<div class="white2" style="width: 200px; float: left">
					<div class="header">12. Pierwsze imię</div>
					<div>
						<input type="text" name="imie" value="${requestScope.customer.imie}" />
					</div>
				</div>
				<div class="white2" style="width: 250px; float: left">
					<div class="header">13. Data urodzenia</div>
					<div>
						<input type="text" name="data" value="${requestScope.customer.data}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 150px; float: left; border-left: none;">
					<div class="header">14. Kraj</div>
					<div>
						<input type="text" name="kraj" value="${requestScope.customer.kraj}"/>
					</div>
				</div>
				<div class="white2" style="width: 300px; float: left">
					<div class="header">15. Województwo</div>
					<div>
						<input type="text" name="woj" value="${requestScope.customer.woj}"/>
					</div>
				</div>
				<div class="white2" style="width: 300px; float: left">
					<div class="header">16. Powiat</div>
					<div>
						<input type="text" name="pow" value="${requestScope.customer.pow}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 180px; float: left; border-left: none;">
					<div class="header">17. Gmina</div>
					<div>
						<input type="text" name="gmina" value="${requestScope.customer.gmina}"/>
					</div>
				</div>
				<div class="white2" style="width: 415px; float: left">
					<div class="header">18. Ulica</div>
					<div>
						<input type="text" name="ulica" value="${requestScope.customer.ulica}"/>
					</div>
				</div>
				<div class="white2" style="width: 80px; float: left">
					<div class="header">19. Nr domu</div>
					<div>
						<input type="text" name="nrDomu" value="${requestScope.customer.nrDomu}"/>
					</div>
				</div>
				<div class="white2" style="width: 80px; float: right">
					<div class="header">20.Nr lokalu</div>
					<div>
						<input type="text" name="nrLokalu" value="${requestScope.customer.nrLokalu}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 350px; float: left; border-left: none;">
					<div class="header">21. Miejscowość</div>
					<div>
						<input type="text" name="miejscowosc" value="${requestScope.customer.miejscowosc}"/>
					</div>
				</div>
				<div class="white2" style="width: 150px; float: left">
					<div class="header">22. Kod pocztowy</div>
					<div>
						<input type="text" name="kod" value="${requestScope.customer.kod}"/>
					</div>
				</div>
				<div class="white2" style="width: 250px; float: left">
					<div class="header">23. Poczta</div>
					<div>
						<input type="text" name="poczta" value="${requestScope.customer.poczta}"/>
					</div>
				</div>
			</div>
		</div>
		<div style="background-color:white">
			<br style="clear: both;" />
			<br/>
			<input type="submit" value="Wstecz" name="prev" style="float:left;">	
			<input type="submit" value="Następny" name="next" style="float:right;">			
		</div>
	</div>
</form>
</body>
</html>
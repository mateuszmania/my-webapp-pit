<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PIT 37 (2 z 4)</title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="header.jsp"/>
</head>
<body>
<form action="second.do" method="post">
	<div class="content">
		<div class="row-2">
			<div class="header3"
				style="width: 110px; margin-left: 10px; float: left;">
				Podstawa prawna:<br> <br> Termin składania:<br>
				Miejsce składania
			</div>
			<div class="header3" style="float: left;">
				Art. 45 ust. 1 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym
				od osób fizycznych (Dz. U. z 2012 r. poz. 361, z późn. zm.),<br>
				zwanej dalej ”ustawą”.<br> Do dnia 30 kwietnia roku
				następującego po roku podatkowym<br> Urząd, o którym mowa w
				art. 45 ustawy, zwany dalej ”urzędem”.
			</div>
		</div>
		<div class="row-3">
			<div class="header" style="margin-left: 10px;">Wybór sposobu
				opodatkowania (zaznaczyć właściwe kwadraty):</div>
			<div>
				<div>
					<a class="header" style="margin-left: 10px; float: left;">6.</a> 
					<input type="checkbox" name="opodatkowanie1" value="indywidualne" id="ind" ${requestScope.customer.opodatkowanie1=='indywidualne'?'checked':'' } style="margin-left: 20px; float: left"> 				 						
						<label for="ind" class="header" style="float: left;">
							1. indywidualnie 
						</label> 
					<input type="checkbox" name="opodatkowanie2" value="wspolnie z malzonkiem zgodnie z wnioskiem, o ktorym mowa w art. 6 ust. 2 ustawy" ${requestScope.customer.opodatkowanie2=='wspolnie z malzonkiem zgodnie z wnioskiem, o ktorym mowa w art. 6 ust. 2 ustawy'?'checked':'' } id="wzm2" style="margin-left: 20px; float: left;"> 
						<label for="wzm2" class="header" style="float: left;"> 
							2. wspólnie z małżonkiem<br> zgodnie z wnioskiem, o którym<br> mowa w art. 6 ust. 2 ustawy
						</label> 
					<input type="checkbox" name="opodatkowanie3" value="wspolnie z malzonkiem" ${requestScope.customer.opodatkowanie3=='wspolnie z malzonkiem'?'checked':''} id="wzm1" style="margin-left: 20px; float: left;"> 
						<label for="wzm1" class="header" style="float: left;"> 
							3. wspólnie z małżonkiem,<br> zgodnie z wnioskiem, o którym<br> mowa w art. 6 ust. 1 ustawy
						</label> 
					<input type="checkbox" name="opodatkowanie4" value="w sposob przewidziany dla osob samotnie wychowujacych dzieci" ${requestScope.customer.opodatkowanie4=='w sposob przewidziany dla osob samotnie wychowujacych dzieci'?'checked':''}
						id="dzieci" style="margin-left: 20px; float: left;">
						 <label	for="dzieci" class="header" style="float: left;"> 
						 	4. w sposób przewidziany<br> dla osób samotnie<br>	wychowujących dzieci
						</label>
				</div>
			</div>
			<div>
				<br style="clear: both;" />
				<div>
					<a class="header" style="margin-left: 10px; float: left;">7.</a> 
					<input type="checkbox" name="opodatkowanie5" value="podatnik" ${requestScope.customer.opodatkowanie5=='podatnik'?'checked':'' } id="podat"
						style="margin-left: 20px; float: left;"> 
					<label
						for="podat" class="header"> w sposób przewidziany w art.
						29 ust. 4 ustawy – podatnik </label>
				</div>
			</div>
			<div style="clear: both;">
				<div>
					<a class="header" style="margin-left: 10px; float: left;">8.</a> <input
						type="checkbox" name="opodatkowanie6" value="malzonek" ${requestScope.customer.opodatkowanie6=='malzonek'?'checked':'' } id="podat2"
						style="margin-left: 20px; float: left;"> <label
						for="podat2" class="header"> w sposób przewidziany w art.
						29 ust. 4 ustawy – małżonek </label>
				</div>
			</div>
		</div>
		<div class="blue">
			<div style="height: 28px;">
				<a style="margin-left: 10px;">A. MIEJSCE I CEL SKŁADANIA ZEZNANIA</a>
			</div>
			<div class="white">
				<div class="header" style="vertical-align: top;">
					9. Urząd, do którego adresowane jest zeznanie 1)
				</div>
				<div>
					<input type="text" name="urzad" value="${requestScope.customer.urzad}" style="top: -5px;" />
				</div>
			</div>
			<div class="white" style="border-top: none;">
				<div>
					<a class="header" style="vertical-align: top; height: 10px;">10. Cel złożenia formularza (zaznaczyć właściwy kwadrat):</a>
				</div>
				<div
					style="height: 20px; margin-left: 250px; position: relative; top: 0px;">
					<div style="vertical-align: top;">
						<input type="checkbox" name="cel" value="zlozenie zeznania" id="zez" ${requestScope.customer.cel=='zlozenie zeznania'?'checked':'' }
							style="float: left;"> <label for="ind" class="header"
							style="float: left;">1. złożenie zeznania </label> 
						<input type="checkbox" name="cel1" value="korekta zeznania" id="kor" ${requestScope.customer.cel1=='korekta zeznania'?'checked':'' }
							style="float: left; margin-left: 50px;"> <label
							for="wzm2" class="header" style="float: left;">2. korekta
							zeznania </label>
					</div>
				</div>
			</div>
		</div>
		<div style="background-color:white">
			<br style="clear: both;" />
			<br/>
			<input type="submit" value="Wstecz" name="prev" style="float:left;">	
			<input type="submit" value="Następny" name="next" style="float:right;">			
		</div>
	</div>
</form>
<script type="text/javascript" language="JavaScript">
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="header.jsp"/>
</head>
<body>
<form action="fourth.do" method="post">
	<div class="content">
		<div class="blue" style="height: 25px; border-top: 1px solid black;">
			<a style="margin-left: 10px;">B. DANE IDENTYFIKACYJNE I AKTUALNY
				ADRES ZAMIESZKANIA</a>
		</div>
		<div class="blue" style="height: 172px">
			<div style="height: 28px;">
				<a style="margin-left: 15px; font-weight: normal;">B.2. DANE
					MAŁŻONKA</a>
			</div>
			<div class="white">
				<div class="white2"
					style="width: 300px; float: left; border-left: none;">
					<div class="header">24. Nazwisko</div>
					<div>
						<input type="text" name="nazwiskoM" value="${requestScope.customer.nazwiskoM}"/>
					</div>
				</div>
				<div class="white2" style="width: 200px; float: left">
					<div class="header">25. Pierwsze imię</div>
					<div>
						<input type="text" name="imieM" value="${requestScope.customer.imieM}"/>
					</div>
				</div>
				<div class="white2" style="width: 250px; float: left">
					<div class="header">26. Data urodzenia</div>
					<div>
						<input type="text" name="dataM" value="${requestScope.customer.dataM}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 150px; float: left; border-left: none;">
					<div class="header">27. Kraj</div>
					<div>
						<input type="text" name="krajM" value="${requestScope.customer.krajM}" />
					</div>
				</div>
				<div class="white2" style="width: 300px; float: left">
					<div class="header">28. Województwo</div>
					<div>
						<input type="text" name="wojM" value="${requestScope.customer.wojM}"/>
					</div>
				</div>
				<div class="white2" style="width: 300px; float: left">
					<div class="header">29. Powiat</div>
					<div>
						<input type="text" name="powM" value="${requestScope.customer.powM}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 180px; float: left; border-left: none;">
					<div class="header">30. Gmina</div>
					<div>
						<input type="text" name="gminaM" value="${requestScope.customer.gminaM}"/>
					</div>
				</div>
				<div class="white2" style="width: 415px; float: left">
					<div class="header">31. Ulica</div>
					<div>
						<input type="text" name="ulicaM" value="${requestScope.customer.ulicaM}" />
					</div>
				</div>
				<div class="white2" style="width: 80px; float: left">
					<div class="header">32. Nr domu</div>
					<div>
						<input type="text" name="nrDomuM" value="${requestScope.customer.nrDomuM}"/>
					</div>
				</div>
				<div class="white2" style="width: 80px; float: right">
					<div class="header">33.Nr lokalu</div>
					<div>
						<input type="text" name="nrLokaluM" value="${requestScope.customer.nrLokaluM}"/>
					</div>
				</div>
			</div>
			<div class="white3">
				<div class="white2"
					style="width: 350px; float: left; border-left: none;">
					<div class="header">34. Miejscowość</div>
					<div>
						<input type="text" name="miejscowoscM" value="${requestScope.customer.miejscowoscM}"/>
					</div>
				</div>
				<div class="white2" style="width: 150px; float: left">
					<div class="header">35. Kod pocztowy</div>
					<div>
						<input type="text" name="kodM" value="${requestScope.customer.kodM}"/>
					</div>
				</div>
				<div class="white2" style="width: 250px; float: left">
					<div class="header">36. Poczta</div>
					<div>
						<input type="text" name="pocztaM" value="${requestScope.customer.pocztaM}"/>
					</div>
				</div>
			</div>
		</div>
		<div style="background-color:white">
			<br style="clear: both;" />
			<br/>
			<input type="submit" value="Wstecz" name="prev" style="float:left;">	
			<input type="submit" value="Wyślij dane" name="next" style="float:right;">			
		</div>
	</div>
</form>
</body>
</html>
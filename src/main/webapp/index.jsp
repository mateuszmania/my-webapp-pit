<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Aplikacja PIT 37</title>
<style>
.header {
	font-family: Calibri;
	font-weight: bold;
	font-size: 20px;
	text-align: left;
	position: relative;
	float: left;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}
.header2 {
	font-family: Calibri;
	font-weight: bold;
	font-size: 18px;
	text-align: left;
	position: relative;
	float: left;
	top: 10px;
	left: 50%;
	transform: translate(-50%, -50%);
}
.header3 {
	font-family: Calibri;
	font-size: 11px;
	text-align: left;
	position: relative;
	float: left;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}
</style>
</head>
<body>
	<div style="width: 800px; height: 350px; background-color: #d7d7d7;">
		<div style="height: 100px; background-color: #FFC300;">
			<div class="header">
				<div><a>Aplikacja PIT 37</a><br>
				<a class="header2">Logowanie</a>
				</div>			
			</div>
		</div>
		<div style="height: 200px;">
			<div class="header">
				<form action="login.do" method="post">
					<table>
						<tr><td>Login:</td><td> <input type="text" name="login"/></td></tr>
						<tr><td>Hasło:</td><td> <input type="password" name="password"/></td></tr>
						<tr><td>Imię:</td><td> <input type="text" name="name" /></td></tr>
						<tr><td>Nazwisko:</td><td> <input type="text" name="surename" /></td></tr>
					</table>
					<br>
					<input class="header2" type="submit" value="Zaloguj"/>
				</form>
			</div>
		</div>
		<div style="height: 50px; background-color: #FFC300;">
			<div class="header3">
				<a>Autor: Mateusz Mania</a>
			</div>
		</div>
	</div>
</body>
</html>
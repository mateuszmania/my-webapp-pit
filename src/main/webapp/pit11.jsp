<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PIT 37 (1 z 4)</title>
<link rel="stylesheet" type="text/css" href="style.css">
<jsp:include page="header.jsp"/>
</head>
<body>
<form action="first.do" method="post">
	<div class="content">
		<div class="row-1">
			<div class="element-1-2">
				<div class="el-1">
					<div class="header">1.Identyfikator podatkowy NIP/numer PESEL
						podatnika</div>
					<div>
						<input type="text" name="pesel" value="${requestScope.customer.pesel}"
							style="width: 100%; height: 100%; border-style: none; text-align: center;" />
					</div>
				</div>
				<div class="el-2">
					<div class="header">2.Identyfikator podatkowy NIP/numer PESEL
						malzonka</div>
					<div>
						<input type="text" name="peselM" value="${requestScope.customer.peselM}"
							style="width: 100%; height: 100%; border-style: none; text-align: center;" />
					</div>
				</div>
			</div>
			<div class="el-3">
				<div class="header">3.Nr dokumentu</div>
				<div>
					<input type="text" name="nrDok"
						style="width: 100%; height: 50px; border-style: none; background-color: transparent; text-align: center;" />
				</div>
			</div>
			<div class="el-4">
				<div class="header">4.Status</div>
				<div>
					<input type="text" name="status"
						style="width: 100%; height: 50px; border-style: none; background-color: transparent; text-align: center;" />
				</div>
			</div>
		</div>
		<div class="yellow">
			<div class="header2">Prawidlowe wypelnienie formularza ulatwi
				wczesniejsze zapoznanie się z broszura informacyjna dostępna w
				urzedach.</div>
			<div
				style="font-family: Calibri; font-weight: bold; text-align: left; font-size: 20px; margin-left: 10px;">
				PIT-37</div>
			<div
				style="font-family: Calibri; font-weight: bold; text-align: center; font-size: 20px; margin-left: 10px;">
				ZEZNANIE O WYSOKOŚCI<br>OSIĄGNIĘTEGO DOCHODU (PONIESIONEJ
				STRATY)<br>
			</div>
			<div
				style="widht: 400px; float: left; margin-left: 220px; font-family: Calibri; font-weight: bold; text-align: right; font-size: 20px;">
				W ROKU PODATKOWYM
				<div class="el-5" style="margin-left: 10px">
					<div class="header">5.Rok</div>
					<div>
						<input type="text" name="rok" value="${requestScope.customer.rok}"
							style="width: 100%; height: 100%; border-style: none; background-color: transparent; text-align: center;" />
					</div>
				</div>
			</div>
			<div class="header"
				style="width: 100%; float: left; margin-left: 10px;">
				Formularz przeznaczony jest dla podatników, którzy w roku
				podatkowym:<br> 1) wyłącznie za pośrednictwem płatnika uzyskali
				przychody ze źródeł położonych na terytorium Rzeczypospolitej
				Polskiej, podlegające opodatkowaniu na ogólnych<br> zasadach
				przy zastosowaniu skali podatkowej, tj. w szczególności z tytułu:
			</div>
			<div>
				<div class="header"
					style="width: 50%; float: left; margin-left: 20px;">
					- wynagrodzeń i innych przychodów ze stosunku służbowego, stosunku<br>
					pracy (w tym spółdzielczego stosunku pracy) oraz pracy nakładczej,<br>
					- emerytur lub rent krajowych (w tym rent strukturalnych, rent
					socjalnych),<br> - świadczeń przedemerytalnych, zasiłków
					przedemerytalnych,<br> - należności z tytułu członkostwa w
					rolniczych spółdzielniach<br> produkcyjnych lub innych
					spółdzielniach zajmujących się produkcją<br> rolną,<br> -
					zasiłków pieniężnych z ubezpieczenia społecznego,<br> -
					stypendiów
				</div>
				<div class="header" style="width: 45%; float: left;">
					- przychodów z działalności wykonywanej osobiście (między innymi z
					umów<br> zlecenia, kontraktów menedżerskich, zasiadania w
					radach nadzorczych,<br> pełnienia obowiązków społecznych,
					działalności sportowej),<br> - przychodów z praw autorskich i
					innych praw majątkowych,<br> - świadczeń wypłaconych z
					Funduszu Pracy lub z Funduszu<br> Gwarantowanych Świadczeń
					Pracowniczych,<br> - należności za pracę przypadających
					tymczasowo aresztowanym oraz skazanym,<br> - należności z
					umowy aktywizacyjnej
				</div>
				<div class="header"
					style="width: 95%; float: left; margin-left: 10px;">
					2) nie prowadzili pozarolniczej działalności gospodarczej oraz
					działów specjalnych produkcji rolnej opodatkowanych na ogólnych
					zasadach przy zastosowaniu skali podatkowej,<br>3) nie są
					obowiązani doliczać do uzyskanych dochodów dochodów małoletnich
					dzieci,<br>4) nie obniżają dochodów o straty z lat ubiegłych.
				</div>
			</div>
		</div>
		<div style="background-color:white">
			<br style="clear: both;" />
			<br/>
			<input type="submit" value="Wstecz" name="prev" style="float:left;">	
			<input type="submit" value="Nastepny" name="next" style="float:right;">			
		</div>
	</div>
</form>
</body>
</html>
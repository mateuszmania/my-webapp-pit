<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PIT 37 - wynik</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<form action="final.do" method="post">
		<div class="content">
			<div class="row-1">
				<div class="element-1-2">
					<div class="el-1">
						<div class="header">1.Identyfikator podatkowy NIP/numer
							PESEL podatnika</div>
						<div>
							<input type="text" name="pesel" value="${requestScope.customer.pesel}" disabled="disabled"
								style="width: 100%; height: 100%; border-style: none; text-align: center;" />
						</div>
					</div>
					<div class="el-2">
						<div class="header">2.Identyfikator podatkowy NIP/numer
							PESEL małżonka</div>
						<div>
							<input type="text" name="peselM" value="${requestScope.customer.peselM}" disabled="disabled"
								style="width: 100%; height: 100%; border-style: none; text-align: center;" />
						</div>
					</div>
				</div>
				<div class="el-3">
					<div class="header">3.Nr dokumentu</div>
					<div>
						<input type="text" name="nrDok" disabled="disabled"
							style="width: 100%; height: 50px; border-style: none; background-color: transparent; text-align: center;" />
					</div>
				</div>
				<div class="el-4">
					<div class="header">4.Status</div>
					<div>
						<input type="text" name="status" disabled="disabled"
							style="width: 100%; height: 50px; border-style: none; background-color: transparent; text-align: center;" />
					</div>
				</div>
			</div>
			<div class="yellow">
				<div class="header2">Prawidłowe wypełnienie formularza ułatwi
					wcześniejsze zapoznanie się z broszurą informacyjną dostępną w
					urzędach.</div>
				<div
					style="font-family: Calibri; font-weight: bold; text-align: left; font-size: 20px; margin-left: 10px;">
					PIT-37</div>
				<div
					style="font-family: Calibri; font-weight: bold; text-align: center; font-size: 20px; margin-left: 10px;">
					ZEZNANIE O WYSOKOŚCI<br>OSIĄGNIĘTEGO DOCHODU (PONIESIONEJ
					STRATY)<br>
				</div>
				<div
					style="widht: 400px; float: left; margin-left: 220px; font-family: Calibri; font-weight: bold; text-align: right; font-size: 20px;">
					W ROKU PODATKOWYM
					<div class="el-5" style="margin-left: 10px">
						<div class="header">5.Rok</div>
						<div>
							<input type="text" name="rok" value="${requestScope.customer.rok}" disabled="disabled"
								style="width: 100%; height: 100%; border-style: none; background-color: transparent; text-align: center;" />
						</div>
					</div>
				</div>
				<div class="header"
					style="width: 100%; float: left; margin-left: 10px;">
					Formularz przeznaczony jest dla podatników, którzy w roku
					podatkowym:<br> 1) wyłącznie za pośrednictwem płatnika
					uzyskali przychody ze źródeł położonych na terytorium
					Rzeczypospolitej Polskiej, podlegające opodatkowaniu na ogólnych<br>
					zasadach przy zastosowaniu skali podatkowej, tj. w szczególności z
					tytułu:
				</div>
				<div>
					<div class="header"
						style="width: 50%; float: left; margin-left: 20px;">
						- wynagrodzeń i innych przychodów ze stosunku służbowego, stosunku<br>
						pracy (w tym spółdzielczego stosunku pracy) oraz pracy nakładczej,<br>
						- emerytur lub rent krajowych (w tym rent strukturalnych, rent
						socjalnych),<br> - świadczeń przedemerytalnych, zasiłków
						przedemerytalnych,<br> - należności z tytułu członkostwa w
						rolniczych spółdzielniach<br> produkcyjnych lub innych
						spółdzielniach zajmujących się produkcją<br> rolną,<br>
						- zasiłków pieniężnych z ubezpieczenia społecznego,<br> -
						stypendiów
					</div>
					<div class="header" style="width: 45%; float: left;">
						- przychodów z działalności wykonywanej osobiście (między innymi z
						umów<br> zlecenia, kontraktów menedżerskich, zasiadania w
						radach nadzorczych,<br> pełnienia obowiązków społecznych,
						działalności sportowej),<br> - przychodów z praw autorskich i
						innych praw majątkowych,<br> - świadczeń wypłaconych z
						Funduszu Pracy lub z Funduszu<br> Gwarantowanych Świadczeń
						Pracowniczych,<br> - należności za pracę przypadających
						tymczasowo aresztowanym oraz skazanym,<br> - należności z
						umowy aktywizacyjnej
					</div>
					<div class="header"
						style="width: 95%; float: left; margin-left: 10px;">
						2) nie prowadzili pozarolniczej działalności gospodarczej oraz
						działów specjalnych produkcji rolnej opodatkowanych na ogólnych
						zasadach przy zastosowaniu skali podatkowej,<br>3) nie są
						obowiązani doliczać do uzyskanych dochodów dochodów małoletnich
						dzieci,<br>4) nie obniżają dochodów o straty z lat ubiegłych.
					</div>
				</div>
			</div>
			<div class="row-2">
				<div class="header3"
					style="width: 110px; margin-left: 10px; float: left;">
					Podstawa prawna:<br> <br> Termin składania:<br>
					Miejsce składania
				</div>
				<div class="header3" style="float: left;">
					Art. 45 ust. 1 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym
					od osób fizycznych (Dz. U. z 2012 r. poz. 361, z późn. zm.),<br>
					zwanej dalej ”ustawą”.<br> Do dnia 30 kwietnia roku
					następującego po roku podatkowym<br> Urząd, o którym mowa w
					art. 45 ustawy, zwany dalej ”urzędem”.
				</div>
			</div>
			<div class="row-3">
				<div class="header" style="margin-left: 10px;">Wybór sposobu
					opodatkowania (zaznaczyć właściwe kwadraty):</div>
				<div>
					<div>
						<a class="header" style="margin-left: 10px; float: left;">6.</a> 
						<input
							type="checkbox" name="opodatkowanie1" value="indywidualne"
							id="ind"
							${requestScope.customer.opodatkowanie1=='indywidualne'?'checked':'' }
							style="margin-left: 20px; float: left;" disabled="disabled"> <label for="ind"
							class="header" style="float: left;"> 1. indywidualnie </label> 
						<input
							type="checkbox" name="opodatkowanie2"
							value="wspolnie z malzonkiem zgodnie z wnioskiem, o ktorym mowa w art. 6 ust. 2 ustawy"
							id="wzm"
							${requestScope.customer.opodatkowanie2=='wspolnie z malzonkiem zgodnie z wnioskiem, o ktorym mowa w art. 6 ust. 2 ustawy'?'checked':'' }
							style="margin-left: 20px; float: left;" disabled="disabled"> <label
							for="wzm2" class="header" style="float: left;"> 2.
							wspólnie z małżonkiem<br> zgodnie z wnioskiem, o którym<br>
							mowa w art. 6 ust. 2 ustawy
						</label> 
						<input type="checkbox" name="opodatkowanie3"
							value="wspolnie z malzonkiem"
							id="wzm1" ${requestScope.customer.opodatkowanie3=='wspolnie z malzonkiem'?'checked':'' } style="margin-left: 20px; float: left;" disabled="disabled"> <label
							for="wzm1" class="header" style="float: left;"> 3.
							wspólnie z małżonkiem,<br> zgodnie z wnioskiem, o którym<br>
							mowa w art. 6 ust. 1 ustawy
						</label> <input type="checkbox" name="opodatkowanie4"
							value="w sposob przewidziany dla osob samotnie wychowujacych dzieci" ${requestScope.customer.opodatkowanie4=='w sposob przewidziany dla osob samotnie wychowujacych dzieci'?'checked':''}
							id="dzieci" style="margin-left: 20px; float: left;" disabled="disabled"> <label
							for="dzieci" class="header" style="float: left;"> 4. w
							sposób przewidziany<br> dla osób samotnie<br>
							wychowujących dzieci
						</label>
					</div>
				</div>
				<div>
					<br style="clear: both;" />
					<div>
						<a class="header" style="margin-left: 10px; float: left;">7.</a> 
						<input type="checkbox" name="opodatkowanie5" value="podatnik" ${requestScope.customer.opodatkowanie5=='podatnik'?'checked':'' } id="podat" style="margin-left: 20px; float: left;" disabled="disabled"> 
						<label for="podat" class="header"> w sposób przewidziany w art. 29 ust. 4 ustawy – podatnik </label>
					</div>
				</div>
				<div style="clear: both;">
					<div>
						<a class="header" style="margin-left: 10px; float: left;">8.</a> <input
							type="checkbox" name="opodatkowanie6"
							value="malzonek" ${requestScope.customer.opodatkowanie6=='malzonek'?'checked':'' } id="podat2" style="margin-left: 20px; float: left;" disabled="disabled"> <label
							for="podat2" class="header"> w sposób przewidziany w art.
							29 ust. 4 ustawy – małżonek </label>
					</div>
				</div>
			</div>
			<div class="blue">
				<div style="height: 28px;">
					<a style="margin-left: 10px;">A. MIEJSCE I CEL SKŁADANIA
						ZEZNANIA</a>
				</div>
				<div class="white">
					<div class="header" style="vertical-align: top;">9. Urząd, do
						którego adresowane jest zeznanie 1)</div>
					<div>
						<input type="text" name="urzad" style="top: -5px;" 
							value="${requestScope.customer.urzad}" disabled="disabled" />
					</div>
				</div>
				<div class="white" style="border-top: none;">
					<div>
						<a class="header" style="vertical-align: top; height: 10px;">10.
							Cel złożenia formularza (zaznaczyć właściwy kwadrat):</a>
					</div>
					<div
						style="height: 20px; margin-left: 250px; position: relative; top: 0px;">
						<div style="vertical-align: top;">
							<input type="checkbox" name="cel" value="zlozenie zeznania"
								id="zez" ${requestScope.customer.cel=='zlozenie zeznania'?'checked':'' }
								style="float: left;" disabled="disabled"> <label for="ind" class="header"
								style="float: left;">1. złożenie zeznania </label> <input
								type="checkbox" name="cel1" value="korekta zeznania" id="kor"
								${requestScope.customer.cel1=='zlozenie zeznania'?'checked':'' }
								style="float: left; margin-left: 50px;" disabled="disabled"> <label
								for="wzm2" class="header" style="float: left;">2.
								korekta zeznania </label>
						</div>
					</div>
				</div>
			</div>
			<div class="blue" style="height: 25px">
				<a style="margin-left: 10px;">B. DANE IDENTYFIKACYJNE I AKTUALNY
					ADRES ZAMIESZKANIA</a>
			</div>
			<div class="blue" style="height: 172px">
				<div style="height: 28px;">
					<a style="margin-left: 15px; font-weight: normal;">B.1. DANE
						PODATNIKA</a>
				</div>
				<div class="white">
					<div class="white2"
						style="width: 300px; float: left; border-left: none;">
						<div class="header">11. Nazwisko</div>
						<div>
							<input type="text" name="nazwisko" value="${requestScope.customer.nazwisko}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 200px; float: left">
						<div class="header">12. Pierwsze imię</div>
						<div>
							<input type="text" name="imie" value="${requestScope.customer.imie}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 250px; float: left">
						<div class="header">13. Data urodzenia</div>
						<div>
							<input type="text" name="data" value="${requestScope.customer.data}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 150px; float: left; border-left: none;">
						<div class="header">14. Kraj</div>
						<div>
							<input type="text" name="kraj" value="${requestScope.customer.kraj}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 300px; float: left">
						<div class="header">15. Województwo</div>
						<div>
							<input type="text" name="woj" value="${requestScope.customer.woj}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 300px; float: left">
						<div class="header">16. Powiat</div>
						<div>
							<input type="text" name="pow" value="${requestScope.customer.pow}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 180px; float: left; border-left: none;">
						<div class="header">17. Gmina</div>
						<div>
							<input type="text" name="gmina" value="${requestScope.customer.gmina}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 415px; float: left">
						<div class="header">18. Ulica</div>
						<div>
							<input type="text" name="ulica" value="${requestScope.customer.ulica}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 80px; float: left">
						<div class="header">19. Nr domu</div>
						<div>
							<input type="text" name="nrDomu" value="${requestScope.customer.nrDomu}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 80px; float: right">
						<div class="header">20.Nr lokalu</div>
						<div>
							<input type="text" name="nrLokalu" value="${requestScope.customer.nrLokalu}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 350px; float: left; border-left: none;">
						<div class="header">21. Miejscowość</div>
						<div>
							<input type="text" name="miejscowosc" value="${requestScope.customer.miejscowosc}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 150px; float: left">
						<div class="header">22. Kod pocztowy</div>
						<div>
							<input type="text" name="kod" value="${requestScope.customer.kod}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 250px; float: left">
						<div class="header">23. Poczta</div>
						<div>
							<input type="text" name="poczta" value="${requestScope.customer.poczta}" disabled="disabled" />
						</div>
					</div>
				</div>
			</div>
			<div class="blue" style="height: 172px">
				<div style="height: 28px;">
					<a style="margin-left: 15px; font-weight: normal;">B.2. DANE
						MAŁŻONKA</a>
				</div>
				<div class="white">
					<div class="white2"
						style="width: 300px; float: left; border-left: none;">
						<div class="header">24. Nazwisko</div>
						<div>
							<input type="text" name="nazwiskoM" value="${requestScope.customer.nazwiskoM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 200px; float: left">
						<div class="header">25. Pierwsze imię</div>
						<div>
							<input type="text" name="imieM" value="${requestScope.customer.imieM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 250px; float: left">
						<div class="header">26. Data urodzenia</div>
						<div>
							<input type="text" name="dataM" value="${requestScope.customer.dataM}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 150px; float: left; border-left: none;">
						<div class="header">27. Kraj</div>
						<div>
							<input type="text" name="krajM" value="${requestScope.customer.krajM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 300px; float: left">
						<div class="header">28. Województwo</div>
						<div>
							<input type="text" name="wojM" value="${requestScope.customer.wojM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 300px; float: left">
						<div class="header">29. Powiat</div>
						<div>
							<input type="text" name="powM" value="${requestScope.customer.powM}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 180px; float: left; border-left: none;">
						<div class="header">30. Gmina</div>
						<div>
							<input type="text" name="gminaM" value="${requestScope.customer.gminaM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 415px; float: left">
						<div class="header">31. Ulica</div>
						<div>
							<input type="text" name="ulicaM" value="${requestScope.customer.ulicaM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 80px; float: left">
						<div class="header">32. Nr domu</div>
						<div>
							<input type="text" name="nrDomuM" value="${requestScope.customer.nrDomuM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 80px; float: right">
						<div class="header">33.Nr lokalu</div>
						<div>
							<input type="text" name="nrLokaluM" value="${requestScope.customer.nrLokaluM}" disabled="disabled"/>
						</div>
					</div>
				</div>
				<div class="white3">
					<div class="white2"
						style="width: 350px; float: left; border-left: none;">
						<div class="header">34. Miejscowość</div>
						<div>
							<input type="text" name="miejscowoscM" value="${requestScope.customer.miejscowoscM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 150px; float: left">
						<div class="header">35. Kod pocztowy</div>
						<div>
							<input type="text" name="kodM" value="${requestScope.customer.kodM}" disabled="disabled"/>
						</div>
					</div>
					<div class="white2" style="width: 250px; float: left">
						<div class="header">36. Poczta</div>
						<div>
							<input type="text" name="pocztaM" value="${requestScope.customer.pocztaM}" disabled="disabled"/>
						</div>
					</div>
				</div>
			</div>
			<div style="background-color: white">
				<br style="clear: both;" /> <br /> 
				<input type="submit" value="Wstecz" name="prev" style="float: left;"> 
				<input type="submit" value="Wyślij dane" name="next" style="float: right;">
			</div>
		</div>
	</form>
</body>
</html>